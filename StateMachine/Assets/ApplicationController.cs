﻿using UnityEngine;
using UnityEngine.UI;

public class ApplicationController : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private InputField stateInputField;
    [SerializeField] private Button mainMenu;
    [SerializeField] private Button profiler;
    [SerializeField] private Button mainState;
  

    public void OnProfilePress()
    {
        animator.SetBool("IsProfilePressed",true);
    }

    public void OnProfileRelease()
    {
        animator.SetBool("IsProfilePressed",false);
    }
    
    public void OnMainStatePress()
    {
        animator.SetBool("IsMainStatePressed",true);
    }

    public void OnMainStateRelease()
    {
        animator.SetBool("IsMainStatePressed",false);
    }

    public void OnMainMenuPress()
    {
        animator.SetBool("IsMainMenuPressed",true);
    }

    public void OnMainMenulease()
    {
        animator.SetBool("IsMainMenuPressed",false);
    }
    // Update is called once per frame
	void Update ()
	{
        
	    if (animator.GetCurrentAnimatorStateInfo(0).IsName("Main Menu"))
	    {
	        stateInputField.text = "Main Menu";
	        mainMenu.interactable = false;
	        profiler.interactable = true;
	        mainState.interactable = true;
	    }
	    if (animator.GetCurrentAnimatorStateInfo(0).IsName("Profiler"))
	    {
	        stateInputField.text = "Profiler";
	        mainMenu.interactable = true;
	        profiler.interactable = false;
	        mainState.interactable = false;
	    }
	    if (animator.GetCurrentAnimatorStateInfo(0).IsName("Main State"))
	    {
	        stateInputField.text = "Main State";
	        mainMenu.interactable = true;
	        profiler.interactable = false;
	        mainState.interactable = false;
	    }
	}
}
