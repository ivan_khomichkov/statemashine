﻿using Stateless;

namespace Assets
{
    public class StateMashineStatless
    {
        public StateMachine<States, Activity> Mashine { get; }

        public StateMashineStatless()
        {
            if (Mashine == null)
            {
                Mashine = new StateMachine<States, Activity>(States.MainScreen);
            }

            Mashine.Configure(States.MainScreen).Permit(Activity.PressMainState, States.MainState)
                .Permit(Activity.PressProfiler, States.Profiler);

            Mashine.Configure(States.Profiler).Permit(Activity.PressMainScreen, States.MainScreen);

            Mashine.Configure(States.MainState).Permit(Activity.PressMainScreen, States.MainScreen);
        }

        public bool PartentsNotWatching { get; set; } = true;

        public enum States
        {
            MainScreen,
            MainState,
            Profiler
        }

        public enum Activity
        {
            PressMainScreen,
            PressProfiler,
            PressMainState,
        }
    }
}