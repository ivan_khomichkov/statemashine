﻿using Stateless;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public class StatelessApplicationController : MonoBehaviour
    {
        [SerializeField] private InputField stateText;
        [SerializeField] private Button mainScreenButton;
        [SerializeField] private Button mainStateButton;
        [SerializeField] private Button mainProfilerButton;

        private Dictionary<StateMashineStatless.Activity, Button> allButtons = new Dictionary<StateMashineStatless.Activity, Button>();

        private readonly StateMashineStatless mashine = new StateMashineStatless();
        private StateMashineStatless.Activity activity;

        private void Start()
        {
            allButtons.Add(StateMashineStatless.Activity.PressProfiler, mainProfilerButton);
            allButtons.Add(StateMashineStatless.Activity.PressMainState, mainStateButton);
            allButtons.Add(StateMashineStatless.Activity.PressMainScreen, mainScreenButton);
            Debug.Log("<color=red>Start</color>");
        }

        public void OnMainMenuClicked()
        {
            activity = StateMashineStatless.Activity.PressMainScreen;
            mashine.Mashine.Fire(activity);

            mashine.Mashine.OnTransitioned(OnTransitionMade);
        }

        private void OnTransitionMade(StateMachine<StateMashineStatless.States, StateMashineStatless.Activity>.Transition obj)
        {
            foreach (var button in allButtons)
            {
                button.Value.interactable = mashine.Mashine.PermittedTriggers.Contains(button.Key);
            }
        }

        public void OnProfilerClicked()
        {
            activity = StateMashineStatless.Activity.PressProfiler;
            mashine.Mashine.Fire(activity);
        }

        public void OnMainState()
        {
            activity = StateMashineStatless.Activity.PressMainState;
            mashine.Mashine.Fire(activity);
        }

        // Update is called once per frame
        void Update()
        {
            stateText.text = mashine.Mashine.State.ToString();
        }
    }
}
